Mini-Mall 前端电商快速开发框架
===============

当前最新版本：v2.5.3 (发布时间：2023-03-27)

[![输入图片说明](https://img.shields.io/static/v1?label=licents&message=GPL3.0&color=green)](https://gitee.com/leepm/mini-mall/blob/master/LICENSE)
[![输入图片说明](https://img.shields.io/static/v1?label=Author&message=上海旭冉信息科技有限公司&color=blue)](https://wwww.yi-types.com)
[![输入图片说明](https://img.shields.io/static/v1?label=version&message=2.5.2&color=green)](https://wwww.yi-types.com)


项目介绍
-----------------------------------

<h3 align="center">Mini-Mall</h3>


⭐️ Mini-Mall 是将我们的“开单虾”产品的核心功能抽离出来的一套快速开发框架，其核心功能100%开源并且还会根据项目情况不定期的更新，Mini Mall定位成为一套新零售综合电商前端快速开发框架，使用Mini-Mall的模板，你可以快速搭建一套 S2B2C 的私域、公域等电商项目，里面丰富的组件库，可以帮助解决前端项目的60~70%以上的重复工作，让开发更多关注自己的个性化业务。既能快速提高效率，节省研发成本，同时又不失灵活性！

uniapp+vue2技术栈开发的开源跨平台，它内置提供了会员分销， 区域代理， 商品零售、转发赚钱、社区团购、快速开店、电商平台、在线收款等功能的新零售电商系统。



开源不易，你的星星是我们持续更新的最大动力！




### 为什么选择Mini-Mall ?

电商行业标准产品，企业真实项目的实战经验结晶，采用强大弹性的架构设计，简洁的代码，最新的技术栈，全方位适合不同需求的前端研发同学，同时更是中小微企业开发需求的不二选择，可以帮助解决前端项目的60~70%以上的重复工作。




### 商业授权

商业版本与开源版本代码一致，没有区分。

商业授权模式为永久授权，支持永久升级。

商业使用需要授权，授权方式可选择联系下方技术同学。




### 开源须知

1.仅允许用于个人学习研究使用。

2.禁止将本开源的代码和资源进行任何形式任何名义的出售。




目录结构
-----------------------------------
```
项目结构

├───components (公共组件)
│   ├───btn （通用按钮）
│   ├───common （通用方法）
│   ├───cust-switch （通用swich滑块）
│   ├───dialog（通用弹窗）
│   ├───diy （模板自定义组件）
│   ├───field-list （通用表单）
│   ├───goods （商品信息组件）
│   ├───guide-page （引导页）
│   ├───jyf-parser （富文本插件）
│   ├───loading （加载中）
│   ├───mixins （mixins类）
│   ├───myGoodsCommon （数据汇总通用组件）
│   ├───navigationBar （页面头部）
│   ├───noticebar （通知）
│   ├───pop-manager （弹窗基础组件）
│   ├───popup （通用弹窗）
│   ├───popup-increment （增值服务弹窗）
│   ├───popup-input （输入型弹窗）
│   ├───popup-official （公众号弹窗）
│   ├───popup-tips （提示弹窗）
│   ├───popup-wx （开单虾公众号）
│   ├───printing （商品弹窗）
│   ├───robin-editor （富文本编辑器插件）
│   ├───share-bottom （底部分享弹窗）
│   ├───shortcut （联系商家）
│   ├───simple-address （地址联动组件）
│   ├───thorui （thorui UI库）
│   ├───toptips （定位）
│   ├───u-charts （图表库）
│   ├───uni-icons （uniapp基础图标）
│   ├───uni-popup （uniapp基础弹窗）
│   ├───uni-search （uniapp基础搜索框）
│   ├───uni-transition （过渡动画）
│   ├───updata （同步加价组件）
│   ├───waterfall （同步商品组件）
│   └───wxs （通用方法类）
├───pages （业务代码）
│   ├───activity （活动）
│   ├───address （地址管理）
│   ├───article （文章列表）
│   ├───bargain （砍价会场）
│   ├───category （商品分类）
│   ├───coupon （优惠券）
│   ├───dealer （我的代理）
│   ├───flow （购物车）
│   ├───goods （商品详情）
│   ├───index （首页）
│   ├───login （登录页）
│   ├───makeMoney （赚钱店小二）
│   ├───myGoods （商品管理）
│   ├───optimization （优选）
│   ├───order （订单管理）
│   ├───points （积分明细）
│   ├───retailShop （我的店铺）
│   ├───robinEditor （文章编辑）
│   ├───search （商品搜索）
│   ├───sharing （拼团专区）
│   ├───sharp （限时秒杀）
│   ├───shop （门店详情）
│   ├───store （订单核销）
│   ├───user （个人中心二级页面）
│   ├───userIndex （个人中心首页）
│   └───_select （自提门店）
├───static （静态资源）
├───store （缓存）
├───utils （工具类）
└───wxParse （微信⼩程序富⽂本解析组件）

   
```

特色：接入硬件快速单打印机设备“**飞鹅打印机**”



技术架构
-----------------------------------
#### 开发环境

- 语言：Vue2.0

- IDE(前端)： Vscode、HBuilder

- 依赖管理：npm



#### 移动端框架

| 说明       | 框架       | 说明       | 框架    |
| ---------- | ---------- | ---------- | ------- |
| 基础框架   | uni-app    | JS版本     | ES6     |
| 基础JS框架 | Vue.js     | 视频播放器 | 原生video |
| 路由管理   | Vue Router | 状态管理   | Vuex    |
| 基础UI库   | thorui      | UI界面基于 | thorui   |
| 网络请求   | axios      |  css预处理  |    scss     |
| 地图引擎   | map      |     |       |



### 版本升级

```
系统后续会提供多场景解决方案。
更多架构：微服务、Saas、中台等，都会支持。 
```

### 功能模块
```
业务功能
├─首页
│  ├─首页
│  └─优选
├─商品
│  ├─轮播图区域
│  ├─商品基本数据区域（价格、销量、分享、标题）
│  ├─标签
│  ├─商品卖点
│  ├─商品评论
│  ├─商品描述
│  ├─购前说明
│  ├─购买记录
│  └─功能区域
│     ├─首页
│     ├─收藏
│     ├─下载
│     ├─加入购物车
│     └─立即购买
├─分类
│  ├─热销商品
│  ├─标签商品
│  └─分类商品
├─店铺
├─购物车
│  ├─地址
│  ├─优惠劵
│  └─物流费
└─我的
   ├─个人资料
   ├─订单相关
   └─我的服务
   
```



项目效果
----

![输入图片说明](https://leepm.oss-cn-beijing.aliyuncs.com/public-images/kaidanxia_index.png)

![输入图片说明](https://leepm.oss-cn-beijing.aliyuncs.com/public-images/kaidanxia_inner01.png)

![输入图片说明](https://leepm.oss-cn-beijing.aliyuncs.com/public-images/kaidanxia_inner02.png)

![输入图片说明](https://leepm.oss-cn-beijing.aliyuncs.com/public-images/kaidanxia_inner.png)



### 最近更新

##### V2.5.3 更新日志

- [x] 优化用户体验
- [x] 修复已知bug



### 技术文档

* 技术官网：https://www.yi-types.com   https://www.kaidanxia.com
* 开发文档：https://doc.kaidanxia.com
* 在线演示：

![输入图片说明](https://leepm.oss-cn-beijing.aliyuncs.com/public-images/feature_qrcode.png)

### 交流合作

如果你想加入我们的开源交流群，请扫码添加 Mini-Mall 项目团队，加入群聊：
![输入图片说明](https://leepm.oss-cn-beijing.aliyuncs.com/public-images/shawn_company_qrcode.png)



如果你有任何对 Mini-Mall 产品上的想法、意见或建议，或商务上的合作需求，请扫码添加 Mini-Mall 项目团队进一步沟通：
![输入图片说明](https://leepm.oss-cn-beijing.aliyuncs.com/public-images/shawn_huangxing_qrcode.png)

## 捐赠 

如果觉得还不错，请作者喝杯咖啡吧 ☺

